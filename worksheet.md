# Task 0

Clone this repository (well done!)

# Task 1

Take a look at the two repositories:

  * (A) https://bitbucket.org/farleyknight/ruby-git
  * (B) https://bitbucket.org/kennethendfinger/git-repo

And answer the following questions about them:

  * Who made the last commit to repository A?
    # farleyknight - Commit: 4831ddd 2012-12-17
  * Who made the first commit to repository A?
    #  scott Chacon Commit: f5baa11 2007-11-08
  * Who made the first and last commits to repository B?
    # last Shawn O. Pearce - Commite 02ac0a 2012-03-15
    # first The Android Open Source Project - Commit cf31fe9 2008-10-22
  * Are either/both of these projects active at the moment?  🤔 If not, what do you think happened?
    #both do not seem to be active at the moment.
    🤔ruby-git was compleated.  git-repo was abandoned.
  * 🤔 Which file in each project has had the most activity?
    #the main branch.

# Task 2

Setup a new IntelliJ project with a main method that will print the following message to the console when run:

~~~~~
Sheep and Wolves
~~~~~

🤔 Now setup a new bitbucket repository and have this project pushed to that repository.
You will first need to `commit`, then `push`.  Ensure you have setup an appropriate `.gitignore`
file.  The one we have in this repository is a very good start.
